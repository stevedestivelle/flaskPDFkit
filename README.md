# PDF Generation with Flask

This Flask app is a shell to create a PDF generating app.

It uses Flask's Jinja2 templating system to design PDFs via the open source wkhtmltopdf project:

https://wkhtmltopdf.org/

